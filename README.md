## READ THIS BEFORE INSTALLATION

Sometime theres an error reading .env and config.js inside config folder related to connecting server to database using ORM. If you found that, just __write your database setup manually using config.json file on resources folder.__ Copy it to config folder, and try to connect to database like usually!

## INSTALATION
To run the project, please follow the instruction
1.  First, run this command for to install the required package
```sh
npm install 
```
2. Setup your database config using config.js + .env or config.json
3. Run migration
```sh
sequelize db:create
sequelize db:migrate
sequelize db:seed:all
```
4. Make sure everything is ok, then you can run the application, pick one of code below
```sh
npm run dev
npm start
```

## ADITIONAL INFORMATION
To see clearly what this application can do and how to perform it, theres an documentation file based on OpenAPI Spesification. 
