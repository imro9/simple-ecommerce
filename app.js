const express = require("express");
const app = express();
const port = 3000;
const router = require("./routes/routes");
const middleware = require("./middleware/middleware");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(router);
app.use("/", middleware.notFoundError);
app.use("/", middleware.internalError);

app.listen(port, () => {
  console.log(`Server runing at http://localhost:${port}`);
});
