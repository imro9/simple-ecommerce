const { Item, Order, Order_detail } = require("../models");
const date = new Date();

module.exports = async (req, res, next) => {
  const datas = req.body;
  const user_id = req.id;
  const item_id = datas.map((item) => item["item_id"]);
  let stockState = false;
  try {
    const items = await Item.findAll({ raw: true, where: { id: item_id } });

    if (items.length !== item_id.length) {
      return res.status(401).json({ status: "fail", message: "cannot find selected item in our database!" });
    }
    const checkStore = items.every((item) => items[0]["store_id"] === item["store_id"]);
    if (!checkStore) {
      return res.status(401).json({ status: "fail", message: "cannot create order with different store!" });
    }

    items.forEach((item) => {
      datas.forEach((data) => {
        if (item["id"] === data["item_id"]) {
          item.order_quantity = data.quantity;
          if (data.quantity > item.stock) {
            stockState = true;
          }
        }
      });
    });
    if (stockState) {
      return res.status(401).json({ status: "fail", message: "out of stock! reduce your quantity" });
    }

    const total = items.reduce((a, b) => a + b["price"] * b["order_quantity"], 0);
    const newOrder = await Order.create({ user_id, store_id: items[0]["store_id"], total, created_at: date, updated_at: date });
    items.forEach((item) => {
      Order_detail.create({
        user_id,
        order_id: newOrder.id,
        item_id: item.id,
        price: item.price,
        quantity: item.order_quantity,
        sub_total: item.price * item.order_quantity,
      });
    });

    datas.forEach((data) => {
      Item.increment({ stock: -`${data.quantity}` }, { where: { id: data.item_id } });
    });

    return res.status(201).json({ status: "success", message: `Success creating order! your order id is ${newOrder.id}` });
  } catch (error) {
    next(error);
  }
};
