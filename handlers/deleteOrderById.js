const { Item, Order, Order_detail } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const id = parseInt(req.params.id);
    const order = await Order.findOne({ raw: true, where: { id } });
    if (order === null) return next();
    if (order.status === "completed") {
      return res.status(409).json({ status: "fail", message: "cannot cancel order, already completed!" });
    }
    const update_item = await Order_detail.findAll({ raw: true, where: { order_id: id }, attributes: ["item_id", "quantity"] });
    Order_detail.destroy({ where: { order_id: id } });
    Order.destroy({ where: { id } });
    update_item.forEach((data) => {
      Item.increment({ stock: +`${data.quantity}` }, { where: { id: data.item_id } });
    });

    return res.status(200).json({ status: "success", message: "success delete data!" });
  } catch (error) {
    next(error);
  }
};
