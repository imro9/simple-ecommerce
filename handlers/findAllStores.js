const { Store } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const stores = await Store.findAll({ raw: true });
    return res.status(200).json({ status: "success", message: "getting all stores", data: stores });
  } catch (error) {
    next(error);
  }
};
