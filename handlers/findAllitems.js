const { Item, Store } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const items = await Item.findAll({ raw: true, attributes: [["id", "item_id"], "name", "category"], include: [{ model: Store, attributes: ["name"] }] });
    res.status(200).json({ status: "success", message: "getting all item datas", data: items });
  } catch (error) {
    next(error);
  }
};
