const { Order, Order_detail, Item } = require("../models");

module.exports = async (req, res, next) => {
  const order_id = parseInt(req.params.id);
  if (isNaN(order_id)) return next();
  try {
    const result = await Order_detail.findAll({
      raw: true,
      where: { order_id },
      attributes: ["quantity", "sub_total", "rating"],
      include: [
        { model: Order, attributes: ["total", "status", "user_id"] },
        { model: Item, attributes: ["name", "id"] },
      ],
    });
    if (result.length === 0) return next();
    if (result[0]["Order.user_id"] !== req.id) return res.status(403).json({ status: "fail", message: "access to this path is forbidden using your account!" });
    const data = { order_id, total: result[0]["Order.total"], status: result[0]["Order.status"], items: [] };
    result.forEach((value) => {
      const newItem = { item_id: value["Item.id"], name: value["Item.name"], quantity: value["quantity"], sub_total: value["sub_total"], rating: value["rating"] };
      data.items.push(newItem);
    });
    return res.status(200).json({ status: "success", message: `showing order detail id ${order_id}`, data });
  } catch (error) {
    next(error);
  }
};
