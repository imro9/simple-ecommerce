const { Item, Store } = require("../models");

module.exports = async (req, res, next) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) {
    return next();
  }
  try {
    const store = await Store.findAll({ raw: true, where: { id: id }, include: [{ model: Item, where: { store_id: id } }] });
    if (store.length === 0) return next();
    const data = { id: store[0].id, name: store[0].name, address: store[0].address, overal_rating: store[0].overal_rating, items: [] };
    store.forEach((value) => {
      const newItem = { id_item: value["Items.id"], name: value["Items.name"], price: value["Items.price"] };
      data["items"].push(newItem);
    });
    return res.status(200).json({ status: "success", messsage: "get store data by id", data });
  } catch (error) {
    next(error);
  }
};
