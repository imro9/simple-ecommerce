const register = require("./register");
const login = require("./login");
const findAllitems = require("./findAllitems");
const search = require("./search");
const findAllStores = require("./findAllStores");
const findStoreById = require("./findStoreById");
const createNewOrder = require("./createNewOrder");
const findOrderById = require("./findOrderById");
const updateOrderById = require("./updateOrderById");
const deleteOrderById = require("./deleteOrderById");

module.exports = { updateOrderById, findOrderById, deleteOrderById, register, login, findAllitems, search, findAllStores, findStoreById, createNewOrder };
