const { User } = require("../models");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const { secretkey } = process.env;

module.exports = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ raw: true, where: { email: email }, attributes: ["id", "email", "password"] });
    if (user === null) {
      res.status(401).json({ status: "fail", message: "email did not match with any users registed!" });
    } else if (user.password != password) {
      res.status(401).json({ status: "fail", message: "Password wrong!" });
    } else {
      const token = jwt.sign({ id: user.id, email }, secretkey);
      console.log(token);
      res.status(200).json({ status: "success", message: "Login Success!", token });
    }
  } catch (err) {
    next(err);
  }
};
