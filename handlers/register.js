const { User } = require("../models");
const date = new Date();

module.exports = async (req, res, next) => {
  const { name, email, password, address } = req.body;
  const findEmail = await User.findOne({ raw: true, where: { email: email } });
  if (findEmail === null) {
    try {
      User.create({
        name,
        email,
        password,
        address,
        created_at: date,
        updated_at: date,
      });
      return res.status(201).json({ status: "success", message: "Registration Success" });
    } catch (error) {
      next(error);
    }
  }

  return res.status(409).json({ status: "fail", message: "Email already registed!" });
};
