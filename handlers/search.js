const { Item, Store } = require("../models");

module.exports = async (req, res, next) => {
  const id = parseInt(req.query.id);
  const category = req.query.category;
  try {
    if (isNaN(id) && category == undefined) {
      return next();
    } else if (category == undefined) {
      const item = await Item.findOne({
        raw: true,
        where: { id: id },
        include: [{ model: Store }],
      });
      if (item === null) return next();

      const data = { item_id: item.id, name: item.name, stock: item.stock, category: item.category, price: item.price, store_id: item["Store.id"], store: item["Store.name"] };
      res.status(200).json({ status: "success", message: `get data by id ${id}`, data });
    } else if (isNaN(id)) {
      const items = await Item.findAll({
        raw: true,
        where: { category: category },
        include: [{ model: Store }],
      });
      if (items.length === 0) return next();
      const data = [];
      items.forEach((item) => {
        const newItem = { item_id: item.id, name: item.name, stock: item.stock, category: item.category, price: item.price, store_id: item["Store.id"], store: item["Store.name"] };
        data.push(newItem);
      });
      res.status(200).json({ status: "success", message: `get datas by category ${category}`, data });
    }
  } catch (error) {
    next(error);
  }
};
