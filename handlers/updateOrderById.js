const { Order, Order_detail, Store, sequelize } = require("../models");
const date = new Date();

module.exports = async (req, res, next) => {
  const order_id = parseInt(req.params.id);
  const datas = req.body;
  const user_id = req.id;
  if (isNaN(order_id)) return next();
  try {
    const order_data = await Order_detail.findAll({ raw: true, where: { order_id, user_id } });
    if (order_data.length === 0) return res.status(403).json({ status: "fail", message: "access to this path is forbidden using your account!" });
    const order = await Order.findOne({ raw: true, where: { id: order_id } });
    if (order.status == "completed") {
      return res.status(409).json({ status: "fail", message: "cannot update data, this order already completed!" });
    }
    if (order_data.length != datas.length) {
      return res.status(409).json({ status: "fail", message: "number of data did not match!" });
    }

    const isItem_idSame =
      datas
        .map((data) => data.item_id)
        .sort()
        .join(",") ===
      order_data
        .map((data) => data.item_id)
        .sort()
        .join(",");

    if (!isItem_idSame) {
      return res.status(409).json({ status: "fail", message: "item id didnot exist in this order!" });
    }

    datas.forEach((data) => {
      const rating = data.rating > 5 ? 5 : data.rating < 1 ? 0 : data.rating;
      Store.update({ transaction: sequelize.literal("transaction + 1"), overal_rating: sequelize.literal(`((overal_rating * transaction)+ ${rating})/(transaction+1)`) }, { where: { id: order.store_id } });
      Order_detail.update({ rating: rating }, { where: { order_id, item_id: data["item_id"] } });
    });

    Order.update({ status: "completed", updated_at: date }, { where: { id: order_id } });
    return res.status(201).json({ status: "success", message: "your order is completed!" });
  } catch (error) {
    next(error);
  }
};
