const jwt = require("jsonwebtoken");
require("dotenv").config();
const { secretkey } = process.env;

module.exports = (req, res, next) => {
  const token = req.headers["authorization"] || req.headers["Authorization"];
  if (typeof token !== "undefined") {
    jwt.verify(token, secretkey, (err, authData) => {
      if (err) {
        res.status(403).json({ status: "fail", message: "access forbidden, please login corectly!" });
      } else {
        req.email = authData.email;
        req.id = authData.id;
        next();
      }
    });
  } else {
    res.status(403).json({ status: "fail", message: "access forbiden, try login corectly!" });
  }
};
