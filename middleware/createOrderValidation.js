module.exports = (req, res, next) => {
  const datas = req.body;
  let error = false;
  if (!Array.isArray(datas)) {
    return res.status(409).json({ status: "fail", message: "data must be array" });
  }

  datas.forEach((data) => {
    if (error === true) {
      return false;
    } else if (!(data.hasOwnProperty("item_id") && data.hasOwnProperty("quantity"))) {
      error = true;
      return res.status(409).json({ status: "fail", message: "data must be array of object that have item_id  and quantity property" });
    } else if (!data.hasOwnProperty("item_id")) {
      error = true;
      return res.status(409).json({ status: "fail", message: "data must be array of object that have item_id property" });
    } else if (!data.hasOwnProperty("quantity")) {
      error = true;
      return res.status(409).json({ status: "fail", message: "data must be array of object that have quantity property" });
    }
  });

  if (!error) {
    next();
  }
};
