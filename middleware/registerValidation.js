const Validator = require("fastest-validator");
const v = new Validator();

module.exports = (req, res, next) => {
  const schema = {
    email: { type: "email", label: "Email Adress" },
    name: "string|min:5",
    password: "string|min:5|max:10",
    address: { type: "string" },
  };
  const check = v.compile(schema);
  const isValidate = check(req.body);
  if (isValidate !== true) {
    return res.status(400).json({ status: "fail", message: isValidate });
  } else {
    return next();
  }
};
