module.exports = (req, res, next) => {
  const datas = req.body;
  let error = false;
  if (!Array.isArray(datas)) {
    return res.status(409).json({ status: "fail", message: "data must be array" });
  }

  datas.forEach((data) => {
    if (error === true) {
      return false;
    } else if (!(data.hasOwnProperty("item_id") && data.hasOwnProperty("rating"))) {
      error = true;
      return res.status(409).json({ status: "fail", message: "data must be array of object that have item_id  and rating property" });
    } else if (!data.hasOwnProperty("item_id")) {
      error = true;
      return res.status(409).json({ status: "fail", message: "data must be array of object that have item_id property" });
    } else if (!data.hasOwnProperty("rating")) {
      error = true;
      return res.status(409).json({ status: "fail", message: "data must be array of object that have rating property" });
    }
  });

  if (!error) {
    next();
  }
};
