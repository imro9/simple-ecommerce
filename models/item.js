"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Item extends Model {
    static associate(models) {
      Item.belongsTo(models.Store, { foreignKey: "store_id" });
      Item.belongsToMany(models.Store, { through: "Order_details", foreignKey: "item_id" });
    }
  }
  Item.init(
    {
      name: DataTypes.STRING,
      store_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      stock: DataTypes.INTEGER,
      category: DataTypes.STRING,
      price: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Item",
      tableName: "Items",
      underscored: true,
      timestamps: false,
    }
  );

  return Item;
};
