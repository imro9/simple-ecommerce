"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Order_detail extends Model {
    static associate(models) {
      Order_detail.belongsTo(models.Item, { foreignKey: "item_id" });
      Order_detail.belongsTo(models.Order, { foreignKey: "order_id" });
      Order_detail.belongsTo(models.User, { foreignKey: "user_id" });
    }
  }
  Order_detail.init(
    {
      user_id: DataTypes.INTEGER,
      order_id: DataTypes.INTEGER,
      item_id: DataTypes.INTEGER,
      price: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
      sub_total: DataTypes.INTEGER,
      rating: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Order_detail",
      tableName: "Order_details",
      underscored: true,
      timestamps: false,
    }
  );
  return Order_detail;
};
