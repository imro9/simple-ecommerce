"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Store extends Model {
    static associate(models) {
      Store.hasMany(models.Item);
    }
  }
  Store.init(
    {
      name: DataTypes.STRING,
      address: DataTypes.STRING,
      transaction: DataTypes.INTEGER,
      overal_rating: DataTypes.DECIMAL,
    },
    {
      sequelize,
      modelName: "Store",
      tableName: "Stores",
      underscored: true,
      timestamps: false,
    }
  );
  return Store;
};
