const date = new Date();

const users = [
  { name: "Nalendra Praja", email: "nalendrapraja11@gmail.com", password: "binarian123", address: "Jimbaran, Bali", created_at: date, updated_at: date },
  { name: "Jesika", email: "jesika@gmail.com", password: "binarian321", address: "Cimahi, Bandung", created_at: date, updated_at: date },
  { name: "Galang Gemilang", email: "gemilang@gmail.com", password: "binarian333", address: "Cibadak, Sukabumi", created_at: date, updated_at: date },
  { name: "Nadira", email: "nadira@gmail.com", password: "binarian123", address: "Cibureum, Bogor", created_at: date, updated_at: date },
];

const stores = [
  { name: "Dwi Arsana Store ", address: " Kerobokan, Denpasar Utara" },
  { name: "Mahdtoya ", address: " Goa Gong, Jimbaran" },
  { name: "Eligo ", address: " Kebayoran Baru, Jakarta" },
];

const items = [
  { name: "Arduino", store_id: 1, stock: 50, category: "electronic", price: 50000 },
  { name: "Ayam Penyet", store_id: 2, stock: 100, category: "food and beverage", price: 15000 },
  { name: "Jos Susu", store_id: 2, stock: 40, category: "food and beverage", price: 7000 },
  { name: "Solder", store_id: 1, category: "electronic", stock: 20, price: 7000 },
  { name: "Cardigan", store_id: 3, stock: 10, category: "fashion", price: 120000 },
  { name: "Nasi Goreng", store_id: 2, stock: 12, category: "food and beverage", price: 17000 },
  { name: "Levis", store_id: 3, stock: 40, category: "fashion", price: 20000 },
  { name: "Vans Black", store_id: 3, stock: 10, category: "fashion", price: 50000 },
];

module.exports = { users, stores, items };
