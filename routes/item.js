const router = require("express").Router();
const handler = require("../handlers/handler");

router.get("/", handler.findAllitems);
router.get("/search", handler.search);

module.exports = router;
