const router = require("express").Router();
const handler = require("../handlers/handler");
const middleware = require("../middleware/middleware");

router.post("/", middleware.authorization, middleware.createOrderValidation, handler.createNewOrder);
router.get("/:id", middleware.authorization, handler.findOrderById);
router.put("/:id", middleware.authorization, middleware.updateOrderValidation, handler.updateOrderById);
router.delete("/:id", middleware.authorization, handler.deleteOrderById);

module.exports = router;
