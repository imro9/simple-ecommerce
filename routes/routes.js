const router = require("express").Router();
const userRouter = require("./user");
const itemRouter = require("./item");
const storeRouter = require("./store");
const orderRouter = require("./order");

router.use("/user", userRouter);
router.use("/item", itemRouter);
router.use("/store", storeRouter);
router.use("/order", orderRouter);

module.exports = router;
