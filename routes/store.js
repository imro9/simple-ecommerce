const router = require("express").Router();
const handler = require("../handlers/handler");

router.get("/", handler.findAllStores);
router.get("/:id", handler.findStoreById);

module.exports = router;
