const router = require("express").Router();
const handler = require("../handlers/handler");
const middleware = require("../middleware/middleware");

router.post("/register", middleware.registerValidation, handler.register);
router.post("/login", middleware.loginValidation, handler.login);

module.exports = router;
